json.extract! project, :id, :name, :tagline, :description, :vcs, :link, :technologies, :created_at, :updated_at
json.url project_url(project, format: :json)

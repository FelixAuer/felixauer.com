module Colors
  module ColorScheme
    TWO_INCH_BRUSH = {
      background: "bg-green-600",
      text: "text-green-50",
      accent: "text-purple-200",
      accent_hover: "hover:text-purple-100"
    }

    SKINSMATCH = {
      background: "bg-neutral-600",
      text: "text-neutral-100",
      accent: "text-amber-400",
      accent_hover: "hover:text-amber-200"
    }

    FRYDERYK = {
      background: "bg-sky-100",
      text: "text-sky-800",
      accent: "text-sky-500",
      accent_hover: "hover:text-sky-300"
    }

    KALM_KANINE = {
      background: "bg-teal-600",
      text: "text-teal-100",
      accent: "text-yellow-300",
      accent_hover: "hover:text-yellow-500"
    }

    THIS_WEBSITE = {
      background: "bg-emerald-800",
      text: "text-emerald-100",
      accent: "text-cyan-400",
      accent_hover: "hover:text-cyan-200"
    }
  end
end

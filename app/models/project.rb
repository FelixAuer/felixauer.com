class Project < ApplicationRecord
  has_rich_text :description
  def colors
    Colors::ColorScheme.const_get(self.color_scheme)
  end
end

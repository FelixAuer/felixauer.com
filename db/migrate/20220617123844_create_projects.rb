class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.text :tagline
      t.text :description
      t.string :vcs
      t.string :link
      t.text :technologies
      t.text :image, null: true
      t.string :color_scheme

      t.timestamps
    end
  end
end
